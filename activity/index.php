<?php
require_once './code.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP_SCP: S02 activity</title>
</head>
<body>
    <h3>Divisible by 5</h3>
    <p><?php echo divisibleLoop(100) ?></p>

    <h3>Array Manipulation</h3>
    <?php array_push($students, 'John Smith'); ?>
    <p><?php var_dump($students); ?></p>
    <p><?php echo count($students); ?></p>
    <?php array_push($students, 'Jane Smith'); ?>
    <p><?php var_dump($students); ?></p>
    <p><?php echo count($students); ?></p>
    <?php array_shift($students); ?>
    <p><?php var_dump($students); ?></p>
    <p><?php echo count($students); ?></p>

</body>
</html>