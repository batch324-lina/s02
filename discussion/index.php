<?php
require_once './code.php'
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S02: Selection Control Structures and Array Manipulation</title>
</head>
<body>
    <h1>Repetition Control Structures</h1>
    <h2>While Loop</h2>
    <p><?php whileLooop(); ?></p>

    <h2>Do-while Loop</h2>
    <p><?php doWhileLoop(); ?></p>

    <h2>For Loop</h2>
    <p><?php forLoop(); ?></p>

    <h2>Contiue and break</h2>
    <p><?php modifiedForLoop(); ?></p>

    <h1>Array Manipulation</h1>
    <h2>Types of Arrays</h2>
    <h3>Simple Array</h3>
    <ul>
        <!-- php codes/statements can be breakdown using the php tags -->
        <?php foreach ($computerBrands as $brand) {
            ?>
            <!-- PHP includes a shorthand for "php echo tag" -->
            <li><?= $brand;?></li>
            <?php 
        }
        ?>
    </ul>
    <h3>Associative Array</h3>
    <ul>
        <?php foreach ($gradePeriods as $period => $grade) { ?>
            Grade in <li><?= $period; ?> is <?= $grade ?></li>
        <?php } ?>
    </ul>
    <h3>Multidimensional Array</h3>
    <ul>
        <?php
        // Each $heroes will be represented by a $team (accessing the outer array elements)
        foreach ($heroes as $team) {
            // Each $team will be represented by a $member (accessing the inner array elements)
            foreach ($team as $member) {
                ?>
                <li><?= $member ?></li>
            <?php }
        }
        ?>
    </ul>

    <h3>Multi-Dimensional Associative Array</h3>
    <ul>
        <?php
        foreach ($ironManPowers as $label => $powerGroup) {
            foreach ($powerGroup as $power) {
        ?>
        <li><?= "$label: $power"; ?></li>
        <?php
            }
        }
        ?>
    </ul>

    <h1>Mutators</h1>
    <h3>Sorting</h3>
    <p><?php print_r($sortedBrands); ?></p>
    <h3>Reverse Sorting</h3>
    <p><?php print_r($reverseSortedBrands); ?></p>
    <h3>Push</h3>
    <?php array_push($computerBrands, 'apple'); ?>
    <p><?php print_r($computerBrands); ?></p>
    <h3>Unshift</h3>
    <?php array_unshift($computerBrands, 'Dell'); ?>
    <p><?php print_r($computerBrands); ?></p>
    <h3>Pop</h3>
    <?php array_pop($computerBrands); ?>
    <p><?php print_r($computerBrands); ?></p>
    <h3>Shift</h3>
    <?php array_shift($computerBrands); ?>
    <p><?php print_r($computerBrands); ?></p>
    <h3>Count</h3>
    <p><?php echo count($computerBrands); ?></p>

    <h3>In array</h3>
    <p><?php var_dump(in_array('Acer', $computerBrands)); ?>;</p>
</body>
</html>