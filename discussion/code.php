<?php
// [Section] Repetition Control Structures
// It is to execute code multiple times

function whileLooop(){
    $count = 5;
    while ($count !== 0) {
        echo $count. '<br/>';
        $count--;
    }
}

/* function whileLooop1($count){
    while ($count !== 0) {
        echo $count. '<br/>';
        $count--;
    }
} */

// Do-Ehile Loop
// A do while loop works a lot like while loop. The only difference is that , do while guarantee that the code will run/executed at least once

function doWhileLoop(){
    $count = 20;
    
    do {
        echo $count. '<br/>';
        $count--;
    } while ($count > 20);
}

// For loop
/* 
for (initial value; condition; iteration/decrementation) {
    code block
}
 */

 function forLoop(){
    for($count = 0; $count <=20; $count++){
     echo $count. '<br/>';   
    }
 }

//  Continue and Break Statement
// "Continue" is a keyword that allows the code to go to the nesxt loop without finishing the current code block
// "Break" on the other hand is a keyword that stops the execution of the current loop.

function modifiedForLoop(){
    for($count = 0; $count <= 20; $count++){
        // If the count is divisible by 2, do this:
        if ($count % 2 === 0){
            continue;
        }
        // If not just continue the iteration
        echo $count. '<br/>';
        // If the count is greater than 10, do this:
        if($count > 10) {
            break;
        }
    }
}

// [Section] Array Manipulation
// An array is a kind of variable that can hold more than one value
// Arrays are declared using array() function or square brackets'[]'
// Inthe earlier version of php,    we cannot use [], but as of php 5.4 we can use the short array syntax which replace array() with []

$studentNumbers = array('2020-1923', '2020-1924', '2020-1965', '2020-1926');

// Berfore the php 5.4
$studentNumbers = ['2020-1923', '2020-1924', '2020-1965', '2020-1926'];
// After the php 5.4
// Simple Arrays
$grade = [98.5, 94.3, 89.2, 90.1];

$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

$tasks = [
    'drink html',
    'eat php',
    'inhale cc',
    'bake javascript'
];

// Associative Array
// Associative Array differ from array in the sense that associative arrays uses description names in naming the element/values (key=>valuepair)
// Double Arrow Operator (=>) - an assignment operator that is commonly used in the creation of associative array

$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' => 94.3, 'thirdGrading' => 89.2, 'fourthGrading' => 90.1];

// Two-dimensional Array
// Two dimensional array is commonly used in image processing, good example of this is our viewing screen that uses multidimensional array of pixels

$heroes = [
    [
        'iron man',
        'thor',
        'hulk'
    ],
    [
        'wolverine',
        'cyclops',
        'jean grey'
    ],
    [
        'batman',
        'superman',
        'wonder woman'
    ]
    ];

// Two-Dimensional Assocciative Array

$ironManPowers = [
    'regular' => [
        'repulsor blast',
        'rocket punch'
    ],
    'signature' => [
        'unibeam'
    ]
    ];

    // Array Iterative method: foreach();
    // foreach ($heroes as $hero) {
    //     print_r($hero);
    // }

    // [Section] Array Mutators
    // Array Mutators modify the contents of an array

    // Array Sorting

    $sortedBrands = $computerBrands;
    $reverseSortedBrands = $computerBrands;

    sort($sortedBrands);

    rsort($reverseSortedBrands);
?>